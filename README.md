# QCMix2017
Projet de Génie Logiciel - Master 1 Informatique
Université Aix Marseille - Campus de Luminy

Sujet : QCMix

Équipe : PLAID (Puissance Loyauté Acharnement Intelligence & Débilité)

Membres : ENTAKLI Romain - GHIOTTO Matis - PAPADOPOULOS Cyril - BAGHAFOR Raphaël

Durée : 10 semaines


# Dépendances
- Production:
  - Java 8
  - LibreOffice/OpenOffice/Microsoft Word
  - JavaFX, si ça ne build/run pas et que vous n'arrivez pas à importer JavaFX, ça doit venir de là
      - Si JavaFX n'est pas installé, une des manières de faire est de télécharger le dernier JDK via oracle.com, décompresser l'archive et de faire pointer les variables d'environnement JAVA_HOME et JFXRT_HOME vers le répertoire créé.
      - Si encore échec à ce niveau-là, un des moyens est de créer un nouveau projet puis d'importer les classes manuellement sur Eclipse (via Import Project).
      - Sinon, pour tester l'executable directement, on vous fournit un JAR à lancer en double clic directement.


# Setup du projet
      - Execution via Eclipse possible
      - Fichier JAR executable fonctionnel


# Documentation
Voir le [wiki](https://framagit.org/wickedsnitch/QCMix2017.git) et la Javadoc.
Voir le [projet](https://git.framasoft.org/easyProg/QCMix) de l'année dernière.
Voir le manuel utilisateur présent dans le repositoire pour utiliser le logiciel dans de bonnes conditions

# License
This program is released under the GNU GPL version 3 or any later version (GPLv3+)
